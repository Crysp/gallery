package com.example.gimages;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {

	private Context context;
	private ImageView[] thumbs;
	private String[] paths;
	private ArrayList<String> str;
	private final static String SDCARD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
	
	public ImageAdapter(Context c) {
		this.context = c;
		this.str = new ArrayList<String>();
	}
	
	@Override
	public int getCount() {
		return str.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(220, 220));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(1, 1, 1, 1);
            System.out.println("convertView == null");
        } else {
            imageView = (ImageView) convertView;
            System.out.println("convertView != null");
        }

        Bitmap bmImg = BitmapFactory.decodeFile(str.get(position));
        imageView.setImageBitmap(bmImg);
        return imageView;
	}

	public void add(String imgPath) {
		//this.paths[this.paths.length] = new String(imgPath);
		str.add(imgPath);
	}

}
