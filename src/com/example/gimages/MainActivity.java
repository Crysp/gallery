package com.example.gimages;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {
	
	private EditText searchField;
	private EditText content;
	private Button searchButton;
	private TextView status;
	private ProgressBar progress;
	private Thread t;
	private JSONObject json;
	private JSONArray jarr;
	private GridView gallery;
	private ImageAdapter iAdapter;
	private final static String SDCARD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
	private int current = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        this.iAdapter = new ImageAdapter(this);
        
        this.searchField = (EditText) findViewById(R.id.searchField);
        this.searchButton = (Button) findViewById(R.id.searchButton);
        this.gallery = (GridView) findViewById(R.id.gallery);
        this.status = (TextView) findViewById(R.id.status);
        this.progress = (ProgressBar) findViewById(R.id.progress);
        
        gallery.setAdapter(iAdapter);
        
        final Handler h = new Handler() {
        	@Override
        	public void handleMessage(Message msg) {
        		switch(msg.what) {
        		case 1:
        			status.setText("Ready");
        			gallery.setAdapter(iAdapter);
        			break;
        		default:
        			status.setText("Downloading: " + current + "/" + jarr.length());
        			progress.setProgress(msg.what);
        			break;
        		}
        	}
        };
        
        t = new Thread() {
        	@Override
        	public void run() {
        		//h.sendEmptyMessage((int)(total/size*100));
        		try {
        			android.util.Log.v("Connection", "Get JSON from Google");
					URL url = new URL("https://ajax.googleapis.com/ajax/services/search/images?" + 
										"v=1.0&start=0&rsz=8&q=" + searchField.getText().toString().replaceAll(" ", "%20"));
					URLConnection connection = url.openConnection();
					
					String line;
					StringBuilder builder = new StringBuilder();
					BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
					while((line = reader.readLine()) != null) {
						builder.append(line);
					}
					
					json = new JSONObject(builder.toString());
					
					JSONObject ob = json.getJSONObject("responseData");
					jarr = ob.getJSONArray("results");
					for(int i=0; i<jarr.length(); i++) {
						current++;
						int count = 0;
						try {
							android.util.Log.v("Connection", "Downloading: " + jarr.getJSONObject(i).getString("url"));
							url = new URL(jarr.getJSONObject(i).getString("url"));
		        			connection = url.openConnection();
		        			connection.connect();
		        			// file length
		        			int size = connection.getContentLength();
		        			
		        			InputStream input = new BufferedInputStream(url.openStream(), 8192);
		        			OutputStream output = new FileOutputStream(SDCARD_PATH + "/image_" + i + ".jpg");
		        			
		        			byte data[] = new byte[1024];
		        			long total = 0;
		        			
		        			while((count = input.read(data)) != -1) {
		        				total += count;
		        				h.sendEmptyMessage((int)(total/size*100));
		        				output.write(data, 0, count);
		        			}
		        			
		        			output.flush();        			
		        			output.close();
		        			input.close();
					        iAdapter.add(SDCARD_PATH + "/image_" + i + ".jpg");
					        //gallery.setAdapter(iAdapter);
					    } catch (IOException e) {
					        e.printStackTrace();
					    }
					}
					
					h.sendEmptyMessage(1);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        };
        
        searchButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				t.start();
			}
        	
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


	@Override
	public void onClick(View v) {
		
	}
    
}
